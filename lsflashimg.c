#include "crypto_hash.h"
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

enum {
  PAYLOAD = 0x00020000,
  Z_SIZE  = 0x01000000,
  BLK_ALIGN = 4096
};

struct segment {
  int32_t  x_size; // Extracted size.
  int32_t  z_size; // Compressed size.
  uint64_t addr  ; // Load address.
  uint32_t flags ;
  uint32_t reserved;
  uint8_t  hash[crypto_hash_BYTES]; // Integrity check.
};

static int decodeLz4Block(const char *src, char *dst, int compressedSize, int dstCapacity) {
  uint8_t const *inp = (const void *)src;
  int j = 0;
  for (int i = 0; i < compressedSize;) {
    int token = inp[i++];

    // Literals
    int literals_length = token >> 4;
    if (literals_length) {
      int l = literals_length + 240;
      while (l == 255) {
        l = inp[i++];
        literals_length += l;
      }

      // Copy the literals
      if (j + literals_length > dstCapacity) { return -1; }
      memcpy(dst + j, inp + i, literals_length);
      j += literals_length;
      i += literals_length;

      // End of buffer?
      if (i == compressedSize) { return j; }
    }

    // Match copy
    // 2 bytes offset (little endian)
    int offset = inp[i++];
    offset += inp[i++] << 8;

    if (0 && !offset) { continue; } // sequence of just literals is end of block
    if (!(offset && offset <= j)) { return -1; } // 0 is an invalid offset value

    // length of match copy
    int match_length = token & 15;
    int l = match_length + 240;
    while (l == 255) {
      l = inp[i++];
      match_length += l;
    }

    // Copy the match
    int pos = j - offset; // position of the match copy in the current output
    int end = j + match_length + 4; // minmatch = 4
    if (end >= dstCapacity) { return -1; }
    while (j < end) { dst[j++] = dst[pos++]; }
  }
  return j;
}

int main() {
  uint8_t *xbuf;
  uint8_t *zbuf;
  struct segment seg;
  unsigned off = PAYLOAD;

#define BUF (10<<20)
  xbuf = malloc(BUF);
  zbuf = malloc(BUF);
  if (!(xbuf && zbuf)) { return -1; }
  
  if (read(0, xbuf, off) != off) { return 1; }
  for (unsigned len = off; len--; ) {
    if (xbuf[len] != 0xFF) {
      char fname[32];
      snprintf(fname, sizeof(fname), "boot.bin");
      int f = open(fname, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
      if (f < 0) { return 2; }
      ++len;
      if (write(f, xbuf, len) != len) { return 3; }
      close(f);
      break;
    }
  }

  printf("%-11s%-11s%-11s%-11s\n", "Address", "Size", "Compressed", "Flags");
  while (1) {
    if (read(0, &seg, sizeof(seg)) != sizeof(seg)) { return 4; }
    off += sizeof(seg);
    if (!~seg.x_size) { break; }
  
    unsigned compress = seg.x_size != seg.z_size;
    if (read(0, compress ? zbuf : xbuf, seg.z_size) != seg.z_size) { return 5; }
    off += seg.z_size;

    if (seg.z_size >= Z_SIZE) { return  6; }
  //if (compress && LZ4_decompress_safe((void*)zbuf, (void*)xbuf, seg.z_size, seg.x_size) < 0) { return 7; }
    if (compress && decodeLz4Block     ((void*)zbuf, (void*)xbuf, seg.z_size, seg.x_size) < 0) { return 7; }
  
    unsigned next_off = (off + (BLK_ALIGN - 1)) & ~(BLK_ALIGN - 1);
    unsigned pad = next_off - off;
    if (next_off >= Z_SIZE) { return 8; }
    if (pad && read(0, zbuf, pad) != pad) { return 9; }
    off = next_off;
    while (pad--) { if (zbuf[pad] != 0xFF) { return 10; } }
    
    if (1) {
      uint64_t hash[crypto_hash_BYTES / 8];
      crypto_hash((void*)hash, xbuf, seg.x_size);
      if (memcmp(hash, seg.hash, sizeof(hash))) { return 11; }
    }

    if (1) {
      char fname[32];
      snprintf(fname, sizeof(fname), "0x%08lX.bin", seg.addr);
      int f = open(fname, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
      if (f < 0) { return 12; }
      if (write(f, xbuf, seg.x_size) != seg.x_size) { return 13; }
      close(f);
    }

    if (compress) { printf("0x%08lX 0x%08X 0x%08X 0x%08X\n"    , seg.addr, seg.x_size, seg.z_size, seg.flags); }
    else          { printf("0x%08lX 0x%08X            0x%08X\n", seg.addr, seg.x_size            , seg.flags); }
  }

  return 0;
}
