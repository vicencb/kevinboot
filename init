#!/bin/sh
export PATH=/usr/bin:/bin:/usr/sbin:/sbin

mount -t proc     -o noexec,nosuid,nodev             none /proc
mount -t sysfs    -o noexec,nosuid,nodev             none /sys
mount -t devtmpfs -o   exec,nosuid,mode=0755,size=2M none /dev

# event0 gpio-keys       EV_KEY KEY_WAKEUP
# event0 gpio-keys       EV_SW  SW_PEN_INSERTED
# event1 cros_ec         EV_KEY <full keyboard>
# event2 cros_ec_buttons EV_KEY KEY_VOLUMEDOWN
# event2 cros_ec_buttons EV_KEY KEY_VOLUMEUP
# event2 cros_ec_buttons EV_KEY KEY_POWER
# event2 cros_ec_buttons EV_SW  SW_LID
# event2 cros_ec_buttons EV_SW  SW_TABLET_MODE

# Lid is closed, this might have been an unintentional power-on.
if ! evtest --query /dev/input/event2 EV_SW SW_LID ; then
  poweroff -f
fi

# Boot into default when no shift key is pressed.
if evtest --query /dev/input/event1 EV_KEY KEY_LEFTSHIFT ; then
  DEFAULT='/dev/mmcblk1p1'
fi

# The Boot Loader Specification.
parse_boot_entry () {
  echo -n 'unset DEVICETREE INITRD LINUX OPTIONS TITLE; '
  unset DEVICETREE INITRD LINUX OPTIONS TITLE
  while read line ; do
    KEY=$(  echo "${line}" | sed 's%^ *\([a-z][-a-z]*\)  *\(.*\)%\1%')
    VALUE=$(echo "${line}" | sed 's%^ *\([a-z][-a-z]*\)  *\(.*\)%\2%')
  
    case "${KEY}" in
      "devicetree")
        if [ "${DEVICETREE}" != "" ] ; then
          return
        fi
        DEVICETREE="${1}${VALUE}"
        ;;
      "initrd")
        if [ "${INITRD}" != "" ] ; then
          # TODO: Should multiple INITRDs be supported?
          return
        fi
        INITRD="${1}${VALUE}"
        ;;
      "linux")
        if [ "${LINUX}" != "" ] ; then
          return
        fi
        LINUX="${1}${VALUE}"
      ;;
      "options")
        OPTIONS="${OPTIONS} ${VALUE}"
        ;;
      "title")
        TITLE="${VALUE}"
        ;;
    esac
  done < "${2}"

  if [ "${DEVICETREE}" = "" ] || [ ! -e "${DEVICETREE}" ] ; then
    return
  fi
  if [ "${LINUX}" = "" ] || [ ! -e "${LINUX}" ] ; then
    return
  fi
  if [ "${INITRD}" != "" ] && [ ! -e "${INITRD}" ] ; then
    return
  fi

  echo "DEVICETREE='${DEVICETREE}'; INITRD='${INITRD}'; LINUX='${LINUX}'; OPTIONS='${OPTIONS}'; TITLE='${TITLE}';"
}

STR="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
while true ; do
  umount /sysroot 2>/dev/null
  echo
  if [ -n "${DEFAULT}" ] ; then
    BOOT_DEV=${DEFAULT}
    unset DEFAULT
    USE_DEF=1
    TIMEOUT=15
    while [ ${TIMEOUT} -gt 0 -a ! -b ${BOOT_DEV} ] ; do
      echo "Waiting for ${BOOT_DEV} ... ${TIMEOUT}"
      TIMEOUT=$(( ${TIMEOUT} - 1 ))
      sleep 1
    done
  else
    echo '[1m    1: manual[0m'
    echo '[1m    2: poweroff[0m'
    echo '[1m    3: reboot[0m'
    echo '[1m    4: reload[0m'
    IDX=4
    for device in /dev/* ; do
      if [ ! -b "${device}" ] ; then continue ; fi
      BLKID="$(blkid ${device})"
      if [ -z "${BLKID}" ] ; then continue ; fi
      KEY="${STR:${IDX}:1}"
      echo "[1m    ${KEY}: ${BLKID}[0m"
      eval "device${KEY}=${device}"
      IDX=$(( ${IDX} + 1 ))
    done
    read -n1 -p'  Enter a selection: '
    echo
    if [ ${REPLY} = 1 ] ; then
      /bin/sh
      continue
    elif [ ${REPLY} = 2 ] ; then
      poweroff -f
    elif [ ${REPLY} = 3 ] ; then
      reboot   -f
    elif [ ${REPLY} = 4 ] ; then
      continue
    fi
    BOOT_DEV=$(eval echo \${device${REPLY}})
  fi
  if [ -z "${BOOT_DEV}" ] ; then continue ; fi

  mount -o ro ${BOOT_DEV} /sysroot
  if [ ${?} != 0 ] ; then continue ; fi

  if [ -e "/sysroot/loader/entries" ] ; then
    BOOT="/sysroot"
  elif [ -e "/sysroot/boot/loader/entries" ] ; then
    BOOT="/sysroot/boot"
  else
    continue
  fi

  IDX=0
  for file in ${BOOT}/loader/entries/*.conf ; do
    eval $(parse_boot_entry "${BOOT}" "${file}")
    if [ -z "${LINUX}" ] ; then continue ; fi
    echo "[1m    ${STR:${IDX}:1}: ${TITLE}[0m"
    IDX=$(( ${IDX} + 1 ))
  done
  if [ "${IDX}" = "0" ] ; then
    echo 'No system found on device.'
    continue
  fi
  if [ "${USE_DEF}" ] || [ "${IDX}" = "1" ] ; then
    unset USE_DEF
    REPLY=1
  else
    read -n1 -p'  Enter a selection: '
    echo
  fi
  IDX=0
  for file in ${BOOT}/loader/entries/*.conf ; do
    eval $(parse_boot_entry "${BOOT}" "${file}")
    if [ -z "${LINUX}" ] ; then continue ; fi
    if [ "${STR:${IDX}:1}" = "${REPLY}" ] ; then
      kboot "${LINUX}" "${DEVICETREE}" "${INITRD}" "${OPTIONS}"
    fi
    IDX=$(( ${IDX} + 1 ))
  done
done
