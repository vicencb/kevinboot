#include "lz4hc.h"
#include "crypto_hash.h"
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RESERVED_SPACE  0x20000

/*
  http://www.sco.com/developers/gabi/latest/ch4.intro.html
  http://www.sco.com/developers/gabi/latest/ch4.eheader.html
  http://www.sco.com/developers/gabi/latest/ch4.sheader.html
  http://www.sco.com/developers/gabi/latest/ch5.pheader.html
*/
enum { EI_MAG0, EI_MAG1, EI_MAG2, EI_MAG3, EI_CLASS, EI_DATA, EI_VERSION, EI_NIDENT = 16 };
enum { ELFMAG0 = 0x7F, ELFMAG1 = 'E', ELFMAG2 = 'L', ELFMAG3 = 'F' };
enum { ELFCLASSNONE, ELFCLASS32, ELFCLASS64 };
enum { ELFDATANONE, ELFDATA2LSB, ELFDATA2MSB };
enum { ET_NONE, ET_REL, ET_EXEC, ET_DYN, ET_CORE };
enum { EV_NONE, EV_CURRENT };
enum { PT_NULL, PT_LOAD, PT_DYNAMIC, PT_INTERP, PT_NOTE, PT_SHLIB, PT_PHDR, PT_TLS };
enum { SHT_NULL, SHT_PROGBITS, SHT_SYMTAB, SHT_STRTAB, SHT_RELA, SHT_HASH, SHT_DYNAMIC, SHT_NOTE, SHT_NOBITS };
enum { SHF_ALLOC = 2, SHF_COMPRESSED = 0x800 };
typedef struct {
  uint8_t  e_ident[EI_NIDENT];
  uint16_t e_type;
  uint16_t e_machine;
  uint32_t e_version;
  uint32_t e_entry;
  uint32_t e_phoff;
  uint32_t e_shoff;
  uint32_t e_flags;
  uint16_t e_ehsize;
  uint16_t e_phentsize;
  uint16_t e_phnum;
  uint16_t e_shentsize;
  uint16_t e_shnum;
  uint16_t e_shstrndx;
} Elf32_Ehdr;
typedef struct {
  uint8_t  e_ident[EI_NIDENT];
  uint16_t e_type;
  uint16_t e_machine;
  uint32_t e_version;
  uint64_t e_entry;
  uint64_t e_phoff;
  uint64_t e_shoff;
  uint32_t e_flags;
  uint16_t e_ehsize;
  uint16_t e_phentsize;
  uint16_t e_phnum;
  uint16_t e_shentsize;
  uint16_t e_shnum;
  uint16_t e_shstrndx;
} Elf64_Ehdr;

typedef struct {
  uint32_t p_type;
  uint32_t p_offset;
  uint32_t p_vaddr;
  uint32_t p_paddr;
  uint32_t p_filesz;
  uint32_t p_memsz;
  uint32_t p_flags;
  uint32_t p_align;
} Elf32_Phdr;
typedef struct {
  uint32_t p_type;
  uint32_t p_flags;
  uint64_t p_offset;
  uint64_t p_vaddr;
  uint64_t p_paddr;
  uint64_t p_filesz;
  uint64_t p_memsz;
  uint64_t p_align;
} Elf64_Phdr;

typedef struct {
  uint32_t sh_name;
  uint32_t sh_type;
  uint32_t sh_flags;
  uint32_t sh_addr;
  uint32_t sh_offset;
  uint32_t sh_size;
  uint32_t sh_link;
  uint32_t sh_info;
  uint32_t sh_addralign;
  uint32_t sh_entsize;
} Elf32_Shdr;
typedef struct {
  uint32_t sh_name;
  uint32_t sh_type;
  uint64_t sh_flags;
  uint64_t sh_addr;
  uint64_t sh_offset;
  uint64_t sh_size;
  uint32_t sh_link;
  uint32_t sh_info;
  uint64_t sh_addralign;
  uint64_t sh_entsize;
} Elf64_Shdr;

enum { FLAG_COMPRESS = 1 << 0 };
enum { BLK_ALIGN = 4096 };

struct segment {
  uint32_t x_size;
  uint32_t z_size;
  uint64_t addr  ;
  uint32_t flags ;
  uint32_t reserved;
  uint8_t  hash[crypto_hash_BYTES];
  uint8_t *data;
};

struct segments {
  struct segment *seg;
  unsigned        cap;
  unsigned        len;
};

static int seg_alloc(struct segments *segments, unsigned num) {
  if (segments->cap < segments->len + num) {
    segments->cap = segments->len + num;
    segments->cap = (segments->cap + 0xF) & ~0xF;
    segments->seg = realloc(segments->seg, segments->cap * sizeof(*segments->seg));
    if (!segments->seg) {
      fprintf(stderr, "%s.\n", strerror(errno));
      return -1;
    }
  }
  return 0;
}

static int elf_read(char const *file_name, struct segments *segments, uint32_t flags) {
  Elf64_Ehdr eh;

  FILE *fi = fopen(file_name, "rb");
  if (!fi) {
    fprintf(stderr, "Error openning '%s': %s.\n", file_name, strerror(errno));
    return -1;
  }
  if (fread(&eh, EI_NIDENT, 1, fi) != 1) {
    if (feof(fi)) {
      fprintf(stderr, "Not ELF file.\n");
      return -1;
    }
    fprintf(stderr, "Error reading '%s'.\n", file_name);
    return -1;
  }
  if (
    eh.e_ident[EI_MAG0] != ELFMAG0 ||
    eh.e_ident[EI_MAG1] != ELFMAG1 ||
    eh.e_ident[EI_MAG2] != ELFMAG2 ||
    eh.e_ident[EI_MAG3] != ELFMAG3
  ) {
    fprintf(stderr, "Not ELF file.\n");
    return -1;
  }
  unsigned b64 = eh.e_ident[EI_CLASS] == ELFCLASS64;
  if (
    (eh.e_ident[EI_CLASS]  != ELFCLASS32 && !b64) ||
    eh.e_ident[EI_DATA]    != ELFDATA2LSB ||
    eh.e_ident[EI_VERSION] != EV_CURRENT
  ) {
    fprintf(stderr, "Only version %u little-endian ELF files supported.\n", EV_CURRENT);
    return -1;
  }
  if (!*(uint8_t *)&(uint32_t){1}) {
    fprintf(stderr, "Only little-endian hosts supported.\n");
    return -1;
  }
  if (b64) {
    if (fread(((void *)&eh) + EI_NIDENT, sizeof(eh) - EI_NIDENT, 1, fi) != 1) {
      fprintf(stderr, "Error reading '%s'.\n", file_name);
      return -1;
    }
  } else {
    Elf32_Ehdr e32;
    if (fread(((void *)&e32) + EI_NIDENT, sizeof(e32) - EI_NIDENT, 1, fi) != 1) {
      fprintf(stderr, "Error reading '%s'.\n", file_name);
      return -1;
    }
    eh.e_type      = e32.e_type     ;
    eh.e_machine   = e32.e_machine  ;
    eh.e_version   = e32.e_version  ;
    eh.e_entry     = e32.e_entry    ;
    eh.e_phoff     = e32.e_phoff    ;
    eh.e_shoff     = e32.e_shoff    ;
    eh.e_flags     = e32.e_flags    ;
    eh.e_ehsize    = e32.e_ehsize   ;
    eh.e_phentsize = e32.e_phentsize;
    eh.e_phnum     = e32.e_phnum    ;
    eh.e_shentsize = e32.e_shentsize;
    eh.e_shnum     = e32.e_shnum    ;
    eh.e_shstrndx  = e32.e_shstrndx ;
  }

  if (
    eh.e_version   != EV_CURRENT ||
    eh.e_ehsize    != (b64 ? sizeof(Elf64_Ehdr) : sizeof(Elf32_Ehdr)) ||
    eh.e_phentsize != (b64 ? sizeof(Elf64_Phdr) : sizeof(Elf32_Phdr)) ||
    eh.e_shentsize != (b64 ? sizeof(Elf64_Shdr) : sizeof(Elf32_Shdr))
  ) {
    fprintf(stderr, "Unexpected ELF file.\n");
    return -1;
  }

  if (fseek(fi, eh.e_shoff, SEEK_SET)) {
    fprintf(stderr, "Error seeking '%s'.\n", file_name);
    return -1;
  }
  if (seg_alloc(segments, eh.e_shnum)) { return -1; }
  for (unsigned i = 0; i < eh.e_shnum; ++i) {
    Elf64_Shdr sh;
    if (b64) {
      if (fread(&sh, sizeof(sh), 1, fi) != 1) {
        fprintf(stderr, "Error reading '%s'.\n", file_name);
        return -1;
      }
    } else {
      Elf32_Shdr s32;
      if (fread(&s32, sizeof(s32), 1, fi) != 1) {
        fprintf(stderr, "Error reading '%s'.\n", file_name);
        return -1;
      }
      sh.sh_name      = s32.sh_name     ;
      sh.sh_type      = s32.sh_type     ;
      sh.sh_flags     = s32.sh_flags    ;
      sh.sh_addr      = s32.sh_addr     ;
      sh.sh_offset    = s32.sh_offset   ;
      sh.sh_size      = s32.sh_size     ;
      sh.sh_link      = s32.sh_link     ;
      sh.sh_info      = s32.sh_info     ;
      sh.sh_addralign = s32.sh_addralign;
      sh.sh_entsize   = s32.sh_entsize  ;
    }
    if (sh.sh_type != SHT_PROGBITS || !(sh.sh_flags & SHF_ALLOC) || !sh.sh_size) { continue; }
    void *buffer = malloc(sh.sh_size);
    if (!buffer) {
      fprintf(stderr, "%s.\n", strerror(errno));
      return -1;
    }
    long current = ftell(fi);
    if (current < 0) {
      fprintf(stderr, "Could not tell '%s': %s.\n", file_name, strerror(errno));
      return -1;
    }
    if (fseek(fi, sh.sh_offset, SEEK_SET)) {
      fprintf(stderr, "Could not seek '%s': %s.\n", file_name, strerror(errno));
      return -1;
    }
    if (fread(buffer, sh.sh_size, 1, fi) != 1) {
      fprintf(stderr, "Could not read '%s': %s.\n", file_name, strerror(errno));
      return -1;
    }
    if (fseek(fi, current, SEEK_SET)) {
      fprintf(stderr, "Could not seek '%s': %s.\n", file_name, strerror(errno));
      return -1;
    }
    segments->seg[segments->len++] = (struct segment){
      .data   = buffer,
      .addr   = sh.sh_addr,
      .x_size = sh.sh_size,
      .z_size = sh.sh_size,
      .flags  = flags,
    };
  }
  fclose(fi);
  return 0;
}

static long load_file(char const *file_name, void **data) {
  FILE *const fi = fopen(file_name, "r");
  if (!fi) {
    fprintf(stderr, "Error openning '%s': %s.\n", file_name, strerror(errno));
    return -1;
  }
  if (fseek(fi, 0, SEEK_END)) {
    fprintf(stderr, "Error seeking '%s'.\n", file_name);
    return -1;
  }
  long const size = ftell(fi);
  if (size <= 0) {
    fprintf(stderr, "Error checking file size '%s'.\n", file_name);
    return -1;
  }
  if (fseek(fi, 0, SEEK_SET)) {
    fprintf(stderr, "Error seeking '%s'.\n", file_name);
    return -1;
  }
  void *buffer = malloc(size);
  if (!buffer) {
    fprintf(stderr, "%s.\n", strerror(errno));
    return -1;
  }
  if (fread(buffer, size, 1, fi) != 1) {
    fprintf(stderr, "Error reading '%s'.\n", file_name);
    return -1;
  }
  fclose(fi);
  *data = buffer;
  return size;
}

static int bin_read(char const *file_name, struct segments *segments, uint64_t sh_addr, uint32_t flags) {
  void *buffer;
  long const size = load_file(file_name, &buffer);
  if (size < 0 || seg_alloc(segments, 1)) { return -1; }
  segments->seg[segments->len++] = (struct segment){
    .data   = buffer,
    .addr   = sh_addr,
    .x_size = size,
    .z_size = size,
    .flags  = flags,
  };
  return 0;
}

static int sh_cmp(struct segment const *a, struct segment const *b) {
  if (a->addr < b->addr) { return -1; }
  if (a->addr > b->addr) { return +1; }
  return 0;
}

static int pad(FILE *fo, unsigned len) {
  static const uint64_t buf[] = { [0 ... 0x1FF] = ~0 };
  while (len) {
    unsigned chunk = len >= sizeof(buf) ? sizeof(buf) : len;
    len -= chunk;
    if (fwrite(buf, chunk, 1, fo) != 1) {
      fprintf(stderr, "Error writing output file.\n");
      return -1;
    }
  }
  return 0;
}

static int64_t read_num(const char *str) {
  char *endptr;
  unsigned long int tmp = strtoul(str, &endptr, (str[1] | ' ') == 'x' ? 16 : 10);
  int64_t ret = tmp;
  if (endptr[0] || ret < 0) { return -1; }
  return ret;
}

int main(int argc, char **argv) {
  struct segments segments = {};
  FILE *fo = stdout;
  int ret = 0;
  void *boot_data;
  long boot_size = 0;
  unsigned boot_offset = 0;

  if (!argc) { return -1; }
  --argc; ++argv;

  while (argc) {
    if (argv[0][0] != '-') { break; }
    char opt = argv[0][1];
    if (argv[0][2]) { ret = 1; goto err_option; }
    --argc; ++argv;
    switch (opt) {
    default : ret = 2; goto err_option;
    case 'h': goto usage;
    case 'B':{
      if (argc < 2) { ret = 4; goto err_option; }
      if (boot_size) { ret = 5; goto err_option; }
      char const *boot_file = argv[0];
      --argc; ++argv;
      boot_size = load_file(boot_file, &boot_data);
      if (boot_size <= 0 || boot_size > RESERVED_SPACE) { return -1; }
      int64_t tmp = read_num(argv[0]);
      --argc; ++argv;
      boot_offset = tmp;
      if (tmp < 0 || boot_offset != (uint64_t)tmp || boot_offset >= RESERVED_SPACE) { return -1; }
      if (boot_offset + boot_size > RESERVED_SPACE) { return -1; }
    }break;
    case 'e':{
      if (argc < 2) { ret = 5; goto err_option; }
      char const *elf_file = argv[0];
      --argc; ++argv;
      int64_t tmp = read_num(argv[0]);
      --argc; ++argv;
      uint32_t flags = tmp;
      if (tmp < 0 || flags != (uint64_t)tmp) { ret = 8; goto err_option; }
      if (elf_read(elf_file, &segments, flags)) { return -1; }
    }break;
    case 'b':{
      if (argc < 3) { ret = 6; goto err_option; }
      char const *bin_file = argv[0];
      --argc; ++argv;
      int64_t tmp = read_num(argv[0]);
      --argc; ++argv;
      uint32_t flags = tmp;
      if (tmp < 0 || flags != (uint64_t)tmp) { ret = 8; goto err_option; }
      tmp = read_num(argv[0]);
      --argc; ++argv;
      uint64_t bin_address = tmp;
      if (tmp < 0 || bin_address != (uint64_t)tmp) { ret = 7; goto err_option; }
      if (bin_read(bin_file, &segments, bin_address, flags)) { return -1; }
    }break;
    }
  }
  if (argc) {
    if (argc != 1) { ret = 11; goto err_option; }
    fo = fopen(argv[0], "w");
    if (!fo) {
      fprintf(stderr, "Could not open '%s' for writing: %s.\n", argv[0], strerror(errno));
      return -1;
    }
  }

  qsort(segments.seg, segments.len, sizeof(*segments.seg), (int (*)(const void *, const void *))sh_cmp);

  for (unsigned i = 1; i < segments.len; ++i) {
    struct segment *a = &segments.seg[i-1];
    struct segment *b = &segments.seg[i-0];
    if (a->addr + a->x_size > b->addr) {
      fprintf(stderr, "Overlapping segments: 0x%08lX + 0x%04X > 0x%08lX\n", a->addr, a->x_size, b->addr);
      return -1;
    }
  }

  for (unsigned i = 1; i < segments.len;) {
    struct segment *a = &segments.seg[i-1];
    struct segment *b = &segments.seg[i-0];
    if (a->addr + a->x_size == b->addr && a->flags == b->flags) {
      uint64_t new_size = a->x_size + b->x_size;
      void *buffer = a->data;
      buffer = realloc(buffer, new_size);
      if (!buffer) {
        fprintf(stderr, "%s.\n", strerror(errno));
        return -1;
      }
      memcpy(buffer + a->x_size, b->data, b->x_size);
      a->data = buffer;
      a->x_size = new_size;

      --segments.len;
      memmove(&segments.seg[i], &segments.seg[i+1], sizeof(*segments.seg) * (segments.len - i));
    } else {
      ++i;
    }
  }

  for (unsigned i = 0; i < segments.len; ++i) {
    struct segment *a = &segments.seg[i];
    crypto_hash(a->hash, a->data, a->x_size);
  }

  for (unsigned i = 0; i < segments.len; ++i) {
    struct segment *a = &segments.seg[i];
    if (!(a->flags & FLAG_COMPRESS)) { continue; }
    int const srcSize = a->x_size;
    if (srcSize < 0 || a->x_size != ((uint64_t)srcSize)) {
      fprintf(stderr, "Segment too big.\n");
      return -1;
    }
    int dstSize = LZ4_compressBound(srcSize);
    if (dstSize <= 0) {
      fprintf(stderr, "Compression size error.\n");
      return -1;
    }
    void *dst = malloc(dstSize);
    if (!dst) {
      fprintf(stderr, "%s.\n", strerror(errno));
      return -1;
    }
    dstSize = LZ4_compress_HC((void*)a->data, dst, srcSize, dstSize, LZ4HC_CLEVEL_MAX);
    if (dstSize <= 0 || dstSize >= srcSize) {
      fprintf(stderr, "Compression error.\n");
      return -1;
    }
    free(a->data);
    a->data = dst;
    a->z_size = dstSize;
  }

  fprintf(stderr, "%-11s%-11s%-11s%-11s\n", "Address", "Size", "Compressed", "Flags");
  for (unsigned i = 0; i < segments.len; ++i) {
    struct segment *a = &segments.seg[i];
    if (a->flags & FLAG_COMPRESS) { fprintf(stderr, "0x%08lX 0x%08X 0x%08X 0x%08X\n"    , a->addr, a->x_size, a->z_size, a->flags); }
    else                          { fprintf(stderr, "0x%08lX 0x%08X            0x%08X\n", a->addr, a->x_size           , a->flags); }
  }

  uint64_t fo_size = 0;
  if (boot_size) {
    if (pad(fo, boot_offset)) { return 25; }
    if (fwrite(boot_data, boot_size, 1, fo) != 1) { ret = 12; goto w_err; }
    if (pad(fo, RESERVED_SPACE - boot_size - boot_offset)) { return -1; }
    fo_size += RESERVED_SPACE;
  }
  for (unsigned i = 0; i < segments.len; ++i) {
    struct segment *a = &segments.seg[i];
#define SIZEOF_HEADER (4*4 + 8 + crypto_hash_BYTES)
    if (fwrite(a      , SIZEOF_HEADER, 1, fo) != 1) { ret = 13; goto w_err; }
    if (fwrite(a->data, a->z_size    , 1, fo) != 1) { ret = 14; goto w_err; }
    fo_size += SIZEOF_HEADER + a->z_size;
    uint64_t align = (fo_size + (BLK_ALIGN - 1)) & ~(BLK_ALIGN - 1);
    if (pad(fo, align - fo_size)) { return -1; }
    fo_size = align;
    if (0) {
      w_err:
      fprintf(stderr, "Error writing output file.\n");
      return ret;
    }
  }
  if (pad(fo, 4)) { return -1; }
  fprintf(stderr, "size %lu KiB\n", fo_size >> 10);

  if (fo != stdout) { fclose(fo); }
  return 0;

err_option:
  fprintf(stderr, "Invalid parameter.\n");
usage:
  fprintf(stderr, "mkflashimg [-h] [-B boot_file boot_offset] [-e elf_file flags | -b bin_file flags bin_address]* [out_file]\n");
  return ret;
}
