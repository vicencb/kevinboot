ATF_VERSION     := 2.9
LZ4_VERSION     := 1.9.4
LINUX_VERSION   := 6.4.4
BUSYBOX_VERSION := 1.36.1
DTC_VERSION     := 1.7.0
KEXEC_VERSION   := 2.0.26
EVTEST_VERSION  := 1.35
SPIFLASH_VERSION:= master

ATF_DIR      := arm-trusted-firmware-${ATF_VERSION}
LZ4_DIR      := lz4-${LZ4_VERSION}
LINUX_DIR    := linux-${LINUX_VERSION}
BUSYBOX_DIR  := busybox-${BUSYBOX_VERSION}
DTC_DIR      := dtc-${DTC_VERSION}
KEXEC_DIR    := kexec-tools-${KEXEC_VERSION}
EVTEST_DIR   := evtest-evtest-${EVTEST_VERSION}
SPIFLASH_DIR := spiflash-${SPIFLASH_VERSION}
#TOOLCHAIN_DIR:= aarch64-linux-musl-native
#TOOLCHAIN_DIR:= aarch64-linux-musl-cross

ATF_FILE      := v${ATF_VERSION}.tar.gz
LZ4_FILE      := v${LZ4_VERSION}.tar.gz
LINUX_FILE    := linux-${LINUX_VERSION}.tar.xz
#LINUX_GIT     := ${HOME}/sources/linux
BUSYBOX_FILE  := ${BUSYBOX_DIR}.tar.bz2
DTC_FILE      := ${DTC_DIR}.tar.xz
KEXEC_FILE    := ${KEXEC_DIR}.tar.xz
EVTEST_FILE   := ${EVTEST_DIR}.tar.bz2
SPIFLASH_FILE := ${SPIFLASH_DIR}.tar.bz2
TOOLCHAIN_FILE:= ${TOOLCHAIN_DIR}.tgz

BOARD := gru-kevin
#BOARD := sapphire
PLAT  := rk3399
DT    := ${PLAT}-${BOARD}

PROGRAMMER:=linux,bus=0,cs=0
#ROGRAMMER:=bitbang,linux,cs=42,clk=41,mosi=40,miso=39
#ROGRAMMER:=ftdi,l1=1,h4=0,h5=0,h6=0

MAKEFLAGS := --no-builtin-rules
LINUX_CONFIG ?= false
BUSYBOX_CONFIG ?= false

ifneq (${TOOLCHAIN_DIR},)
       TRIPLE                 := aarch64-linux-musl
       TOOLCHAIN              := ${PWD}/autog/${TRIPLE}
export CROSS_COMPILE          := ${TRIPLE}-
export PATH                   := /bin:/usr/bin:${TOOLCHAIN}/bin
else
export PATH                   := /bin:/usr/bin
endif
export HOST_CC                := ${CC}
export CC                     := ${CROSS_COMPILE}${CC}
export LDFLAGS                := -static
export LC_COLLATE             := C
export ARCH                   := arm64
export KCONFIG_NOTIMESTAMP    := 1
export SOURCE_DATE_EPOCH      := 0
export KBUILD_BUILD_TIMESTAMP := @${SOURCE_DATE_EPOCH}
export KBUILD_BUILD_USER      := local
export KBUILD_BUILD_HOST      := host

ATF_INCLUDE := autog/atf/include/export/plat/rockchip/common

ATF:=autog/bl31.elf
LNX:=autog/Image
DTB:=autog/${DT}.dtb
DTS:=autog/${DT}.dts
RFS:=autog/rootfs.cpio.zst

BINARIES+=autog/rootfs/init
BINARIES+=autog/rootfs/bin/kboot
BINARIES+=autog/rootfs/bin/busybox
BINARIES+=autog/rootfs/bin/fdtput
BINARIES+=autog/rootfs/bin/fdtget
BINARIES+=autog/rootfs/bin/kexec
EXTRA_BINARIES+=autog/rootfs/bin/evtest
EXTRA_BINARIES+=autog/rootfs/bin/spiflash

FSBL_CFLAGS+=-Wall
FSBL_CFLAGS+=-Wextra
FSBL_CFLAGS+=-Werror
FSBL_CFLAGS+=-ffreestanding
FSBL_CFLAGS+=-ffunction-sections
FSBL_CFLAGS+=-fdata-sections
FSBL_CFLAGS+=-Os
FSBL_CFLAGS+=-mstrict-align
FSBL_CFLAGS+=-Ifsbl
FSBL_CFLAGS+=-Icb
FSBL_CFLAGS+=-Inacl
FSBL_CFLAGS+=-I${ATF_INCLUDE}

FSBL_LDFLAGS+=--gc-sections

BOOT_OBJS+=autog/cb/sdram.o
BOOT_OBJS+=autog/cb/sdram_params.o
BOOT_OBJS+=autog/musl/memcmp.o
BOOT_OBJS+=autog/musl/memcpy.o
BOOT_OBJS+=autog/musl/memmove.o
BOOT_OBJS+=autog/musl/memset.o
BOOT_OBJS+=autog/nacl/blocks.o
BOOT_OBJS+=autog/nacl/hash.o
BOOT_OBJS+=autog/fsbl/atf.o
BOOT_OBJS+=autog/fsbl/clock.o
BOOT_OBJS+=autog/fsbl/console.o
BOOT_OBJS+=autog/fsbl/gpio.o
BOOT_OBJS+=autog/fsbl/main.o
BOOT_OBJS+=autog/fsbl/memconsole.o
BOOT_OBJS+=autog/fsbl/power.o
BOOT_OBJS+=autog/fsbl/pwm.o
BOOT_OBJS+=autog/fsbl/reset.o
BOOT_OBJS+=autog/fsbl/spi.o
BOOT_OBJS+=autog/fsbl/timer.o
BOOT_OBJS+=autog/fsbl/uart.o
BOOT_OBJS+=autog/fsbl/start.o

MFI_CFLAGS=-Wall
MFI_CFLAGS+=-Wextra
MFI_CFLAGS+=-Werror
MFI_CFLAGS+=-Os
MFI_CFLAGS+=-Inacl
MFI_CFLAGS+=-Iautog/lz4/lib

MFI_SRCS+=mkflashimg.c
MFI_SRCS+=autog/lz4/lib/lz4.c
MFI_SRCS+=autog/lz4/lib/lz4hc.c
MFI_SRCS+=nacl/hash.c
MFI_SRCS+=nacl/blocks.c

LFI_SRCS+=lsflashimg.c
LFI_SRCS+=nacl/hash.c
LFI_SRCS+=nacl/blocks.c

all : autog/flash.img autog/spiflash ; mkdir -p release && cp -a autog/spiflash autog/flash.img release/
.PHONY: all

pkgs/${ATF_FILE}      : ; wget -P pkgs https://github.com/ARM-software/arm-trusted-firmware/archive/${ATF_FILE}
pkgs/${LZ4_FILE}      : ; wget -P pkgs https://github.com/lz4/lz4/archive/${LZ4_FILE}
pkgs/${LINUX_FILE}    : ; wget -P pkgs https://cdn.kernel.org/pub/linux/kernel/v6.x/${LINUX_FILE}
pkgs/${BUSYBOX_FILE}  : ; wget -P pkgs https://busybox.net/downloads/${BUSYBOX_FILE}
pkgs/${DTC_FILE}      : ; wget -P pkgs https://www.kernel.org/pub/software/utils/dtc/${DTC_FILE}
pkgs/${KEXEC_FILE}    : ; wget -P pkgs https://www.kernel.org/pub/linux/utils/kernel/kexec/${KEXEC_FILE}
pkgs/${EVTEST_FILE}   : ; wget -P pkgs https://gitlab.freedesktop.org/libevdev/evtest/-/archive/evtest-${EVTEST_VERSION}/${EVTEST_FILE}
pkgs/${SPIFLASH_FILE} : ; wget -P pkgs https://gitlab.com/vicencb/spiflash/-/archive/master/${SPIFLASH_FILE}
pkgs/${TOOLCHAIN_FILE}: ; wget -P pkgs https://musl.cc/${TOOLCHAIN_FILE}

${ATF_INCLUDE}/plat_params_exp.h : pkgs/${ATF_FILE} atf.patch | autog ${TOOLCHAIN} ; \
  rm -rf autog/${ATF_DIR} autog/atf         && \
  tar -xf pkgs/${ATF_FILE} -C autog         && \
  patch -p1 -d autog/${ATF_DIR} < atf.patch && \
  mv autog/${ATF_DIR} autog/atf             && \
  touch -c $@
autog/lz4/lib/lz4hc.c : autog/lz4/lib/lz4.c
autog/lz4/lib/lz4.c : pkgs/${LZ4_FILE} | autog ; \
  rm -rf autog/${LZ4_DIR} autog/lz4 && \
  tar -xf pkgs/${LZ4_FILE} -C autog && \
  mv autog/${LZ4_DIR} autog/lz4     && \
  touch -c $@
autog/lnx/README : pkgs/${LINUX_FILE} spi1.patch kensing.patch bright.patch keyb_init.patch cros_ec_keyb_remap.patch | autog ${TOOLCHAIN} ; \
  rm -rf autog/${LINUX_DIR} autog/lnx               && \
  if [ "${LINUX_FILE}" ] ; then                        \
    tar -xf pkgs/${LINUX_FILE} -C autog              ; \
  else                                                 \
    mkdir autog/${LINUX_DIR}                        && \
    git -C ${LINUX_GIT} archive "v${LINUX_VERSION}" | tar -C autog/${LINUX_DIR} -x ; \
  fi                                                && \
  rm -rf "autog/${LINUX_DIR}/drivers/gpu/drm/amd"   && \
  sed -i 's@^source "drivers/gpu/drm/amd/@#&@' "autog/${LINUX_DIR}/drivers/gpu/drm/Kconfig" && \
  patch -p1 -d autog/${LINUX_DIR} < spi1.patch      && \
  patch -p1 -d autog/${LINUX_DIR} < kensing.patch   && \
  patch -p1 -d autog/${LINUX_DIR} < keyb_init.patch && \
  patch -p1 -d autog/${LINUX_DIR} < cros_ec_keyb_remap.patch && \
  patch -p1 -d autog/${LINUX_DIR} < bright.patch    && \
  mv autog/${LINUX_DIR} autog/lnx                   && \
  touch -c $@
autog/bb/README : pkgs/${BUSYBOX_FILE} | autog ${TOOLCHAIN} ; \
  rm -rf autog/${BUSYBOX_DIR} autog/bb             && \
  tar -xf pkgs/${BUSYBOX_FILE} -C autog            && \
  mv autog/${BUSYBOX_DIR} autog/bb                 && \
  touch -c $@
autog/dtc/README.md : pkgs/${DTC_FILE}      | autog ${TOOLCHAIN} ; \
  rm -rf autog/${DTC_DIR} autog/dtc                && \
  tar -xf pkgs/${DTC_FILE} -C autog                && \
  mv autog/${DTC_DIR} autog/dtc                    && \
  touch -c $@
autog/kex/AUTHORS   : pkgs/${KEXEC_FILE}    | autog ${TOOLCHAIN} ; \
  rm -rf autog/${KEXEC_DIR} autog/kex              && \
  tar -xf pkgs/${KEXEC_FILE} -C autog              && \
  (cd autog/${KEXEC_DIR} && ./configure --host=${TRIPLE}) && \
  mv autog/${KEXEC_DIR} autog/kex                  && \
  touch -c $@
autog/evt/README.md : pkgs/${EVTEST_FILE}   | autog ${TOOLCHAIN} ; \
  rm -rf autog/${EVTEST_DIR} autog/evt             && \
  tar -xf pkgs/${EVTEST_FILE} -C autog             && \
  (cd autog/${EVTEST_DIR} && ./autogen.sh --host=${TRIPLE}) && \
  mv autog/${EVTEST_DIR} autog/evt                 && \
  touch -c $@
autog/sf/readme.md  : pkgs/${SPIFLASH_FILE} | autog ${TOOLCHAIN} ; \
  rm -rf autog/${SPIFLASH_DIR} autog/sf            && \
  tar -xf pkgs/${SPIFLASH_FILE}       -C autog     && \
  mv autog/${SPIFLASH_DIR} autog/sf                && \
  touch -c $@
autog/sfs/readme.md : pkgs/${SPIFLASH_FILE} | autog ; \
  rm -rf autog/${SPIFLASH_DIR}_static autog/sfs    && \
  tar -x --xform='s/${SPIFLASH_DIR}/${SPIFLASH_DIR}_static/' -f pkgs/${SPIFLASH_FILE} -C autog && \
  mv autog/${SPIFLASH_DIR}_static autog/sfs        && \
  touch -c $@

sources :: ${ATF_INCLUDE}/plat_params_exp.h
sources :: autog/lz4/lib/lz4.c
sources :: autog/lnx/README
sources :: autog/bb/README
sources :: autog/dtc/README.md
sources :: autog/kex/AUTHORS
sources :: autog/evt/README.md
sources :: autog/sf/readme.md
sources :: autog/sfs/readme.md

autog/lnx/.config : autog/lnx/README linux.config ; ( \
    cp linux.config $@                                && \
    ${MAKE} -C autog/lnx olddefconfig                 && \
    if ${LINUX_CONFIG} ; then ${MAKE} -C autog/lnx nconfig ; fi && \
    ${MAKE} -C autog/lnx savedefconfig                && \
    diff -u linux.config autog/lnx/defconfig             \
  ) || (rm -f $@ && false)
autog/bb/.config : autog/bb/README ; \
  ${MAKE} -C autog/bb defconfig > /dev/null && \
  sed -i 's@^# CONFIG_STATIC is not set$$@CONFIG_STATIC=y@ ;  s@^# CONFIG_INSTALL_NO_USR is not set$$@CONFIG_INSTALL_NO_USR=y@ ;  s@^CONFIG_PREFIX="./_install"$$@CONFIG_PREFIX="../../autog/rootfs"@' $@ && \
  if ${BUSYBOX_CONFIG} ; then ${MAKE} -C autog/bb menuconfig && cp -a $@ autog/bb/config.save ; fi

ATF_OUT := release
# DEBUG=1
autog/atf/build/${PLAT}/${ATF_OUT}/bl31/bl31.elf : ${ATF_INCLUDE}/plat_params_exp.h ; \
  ${MAKE} -C autog/atf PLAT=${PLAT} ; \
  rm -f autog/atf/build/${PLAT}/${ATF_OUT}/bl31.bin
${ATF} : autog/atf/build/${PLAT}/${ATF_OUT}/bl31/bl31.elf ; ${CROSS_COMPILE}strip -sR .comment -o $@ $<

autog/lnx/arch/arm64/boot/Image : autog/lnx/.config ; ${MAKE} -C autog/lnx Image dtbs
${LNX} : autog/lnx/arch/arm64/boot/Image ; cp -a $< $@
autog/lnx/arch/arm64/boot/dts/rockchip/.${DT}.dtb.dts.tmp : ${LNX}
${DTS} : autog/lnx/arch/arm64/boot/dts/rockchip/.${DT}.dtb.dts.tmp ; \
  grep -v -e '^#' -e '^$$' autog/lnx/arch/arm64/boot/dts/rockchip/.${DT}.dtb.dts.tmp > $@

autog/bb/busybox         : autog/bb/.config ; ${MAKE} -C autog/bb busybox
autog/rootfs/bin/busybox : autog/bb/busybox ; ${MAKE} -C autog/bb install

autog/dtc/fdtput : autog/dtc/README.md ; ${MAKE} -C autog/dtc NO_VALGRIND=1 STATIC_BUILD=1 fdtput fdtget
autog/dtc/fdtget : autog/dtc/fdtput
autog/rootfs/bin/fdtput : autog/dtc/fdtput ; cp -a autog/dtc/fdtput $@
autog/rootfs/bin/fdtget : autog/dtc/fdtget ; cp -a autog/dtc/fdtget $@

autog/kex/build/sbin/kexec : autog/kex/AUTHORS ; ${MAKE} -C autog/kex
autog/rootfs/bin/kexec : autog/kex/build/sbin/kexec ; cp -a autog/kex/build/sbin/kexec $@

autog/evt/evtest : autog/evt/README.md ; ${MAKE} -C autog/evt
autog/rootfs/bin/evtest : autog/evt/evtest ; cp -a autog/evt/evtest $@

autog/sf/spiflash : autog/sf/readme.md ; ${MAKE} -C autog/sf FTDI=0
autog/rootfs/bin/spiflash : autog/sf/spiflash ; cp -a autog/sf/spiflash $@

autog/sfs/spiflash : autog/sfs/readme.md ; CC=${HOST_CC} CROSS_COMPILE= FTDI=0   ${MAKE} -C autog/sfs
#utog/sfs/spiflash : autog/sfs/readme.md ; CC=${HOST_CC} CROSS_COMPILE= LDFLAGS= ${MAKE} -C autog/sfs
autog/spiflash : autog/sfs/spiflash ; cp -a $< $@

autog/rootfs/init      : init  | autog ; cp -a init  $@
autog/rootfs/bin/kboot : kboot | autog ; cp -a kboot $@

# cpio --reproducible
${RFS} : ${BINARIES} ${EXTRA_BINARIES} | autog ; \
  find autog/rootfs -type f -not -name init -not -name kboot -exec ${CROSS_COMPILE}strip -sR .comment {} + && ( \
    cp -a autog/rootfs autog/rootfs.tmp            && \
    find autog/rootfs.tmp -exec touch -chd @0 {} + && \
    ( cd autog/rootfs.tmp && find . | sort | cpio --quiet -H newc -o ) | zstd --ultra -22 > $@ \
  ) ; rm -rf autog/rootfs.tmp

read  :                   release/spiflash ; doas release/spiflash -p ${PROGRAMMER} -o -r $@
flash : release/flash.img release/spiflash ; doas release/spiflash -p ${PROGRAMMER} -o -w $<
verif : release/flash.img release/spiflash ; doas release/spiflash -p ${PROGRAMMER} -o -v $<
resto : restore_spi.img   release/spiflash ; doas release/spiflash -p ${PROGRAMMER} -o -w $<
erase :                   release/spiflash ; doas release/spiflash -p ${PROGRAMMER} -o -e
.PHONY: flash verif resto erase

autog/mkflashimg : ${MFI_SRCS} | autog ; ${HOST_CC} ${MFI_CFLAGS} ${MFI_SRCS} -o $@
autog/lsflashimg : ${LFI_SRCS} | autog ; ${HOST_CC} ${MFI_CFLAGS} ${LFI_SRCS} -o $@

autog/flash.img  : autog/mkflashimg autog/boot.spi ${ATF} ${LNX} ${DTB} ${RFS} ; \
  autog/mkflashimg \
  -B autog/boot.spi 0    \
  -e ${ATF} 1            \
  -b ${DTB} 1 0x00100000 \
  -b ${LNX} 1 0x00200000 \
  -b ${RFS} 0 0x02000000 \
  $@

autog/boot.elf: fsbl/link.lds ${BOOT_OBJS} ; ${CROSS_COMPILE}ld -T $< ${BOOT_OBJS} ${FSBL_LDFLAGS} -o $@
autog/boot.bin: autog/boot.elf ; ${CROSS_COMPILE}objcopy -O binary $< $@
autog/boot.spi: autog/boot.bin ; mkimage -n ${PLAT} -T rkspi -d $< $@

autog : ; mkdir -p autog/rootfs/bin autog/rootfs/dev autog/rootfs/proc autog/rootfs/sys autog/rootfs/sysroot autog/rootfs/tmp

autog/${DT}.sdt : ${DT}.patch ${DTS} | autog ; patch -p0 -d autog -o - >$@ <$<
autog/${DT}.dst : autog/${DT}.sdt ${RFS} ; \
  RFS_END=$$( printf 0x%08X $$(( 0x02000000 + $$(stat -c %s ${RFS}) )) ) ; \
  sed "s@initrd-end   = <0 0xXXXXXXXX>@initrd-end   = <0 $$RFS_END>@" $< > $@
${DTB} : autog/${DT}.dst ; ./autog/lnx/scripts/dtc/dtc -O dtb \
  -Wno-unit_address_vs_reg         \
  -Wno-avoid_unnecessary_addr_size \
  -Wno-graph_child_address         \
  -o $@ $<

autog/%.o : %.S ${TOOLCHAIN} ; mkdir -p $$(dirname $@) && ${CC} ${FSBL_CFLAGS} -c $< -o $@
autog/%.o : %.c ${TOOLCHAIN} ${ATF_INCLUDE}/plat_params_exp.h ; mkdir -p $$(dirname $@) && ${CC} ${FSBL_CFLAGS} -c $< -o $@

${TOOLCHAIN} : pkgs/${TOOLCHAIN_FILE} | autog ; \
  rm -rf ${TOOLCHAIN} ${TOOLCHAIN}-native    && \
  bsdtar -xf pkgs/${TOOLCHAIN_FILE} -C autog && \
  for i in ar as cpp ld objcopy objdump nm strip ; do \
    [ -e autog/${TOOLCHAIN_DIR}/bin/aarch64-linux-musl-$$i ] || ln -s $$i autog/${TOOLCHAIN_DIR}/bin/aarch64-linux-musl-$$i ; \
  done                                       && \
  mv autog/${TOOLCHAIN_DIR} ${TOOLCHAIN}     && \
  touch -c $@

clean : ; rm -rf autog
distclean : clean ; rm -rf pkgs release
.PHONY: clean distclean
