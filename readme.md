ABOUT
=====

This is a bootloader replacement for the Samsung Chromebook Plus (XE513C24), aka Google Kevin, gru-kevin.

It is intended for booting linux via kexec. No attempt has been made to boot Android with it.

DESCRIPTION
===========

The bootloader is made of several parts:

1. The first stage boot loader (fsbl).
2. Arm-Trusted-Firmware (ATF).
3. Linux.
4. InitRamFS.

The fsbl initializes the minimum required subset of hardware to be able to load and
execute all other stages. This includes a timer, power supplies, clocks, the DRAM and the SPI interface.
This explicitly excludes display, USB, keyboard, touchpad, uSD and eMMC in order to keep it small and simple.
So, it is completely headless, with no user interaction at all.

Then it reads and extracts everything from the SPI flash memory.
Finally it passes control to the ATF.

The ATF switches from EL3 to EL2 and passes control to Linux. It remains resident to provide PSCI services.

Linux boots and executes the `init` script from InitRamFS.
It is configured with display and keyboard drivers to provide a user interface.
It also supports USB, uSD and eMMC drivers to access storage devices to load an OS.
The only filesystem supported is F2FS.

`init` looks for available OS following [The Boot Loader Specification].
The first one found is booted via `kexec`.
If the left shift key is pressed, then it shows a menu with the boot options.

[The Boot Loader Specification]: https://systemd.io/BOOT_LOADER_SPECIFICATION

A sample bootloader entry looks like this:

    title       Linux in eMMC
    options     root=/dev/mmcblk1p2
    linux       /Image
    devicetree  /dtbs/rockchip/rk3399-gru-kevin.dtb
    initramfs   /initramfs.img

DISCLAIMER
==========

This procedure will brick your device! Probably.

On top of the risk of bricking, there is also the risk of unrecoverable hardware damage.

Proceed at your own risk and only after backing-up the SPI-flash memory in a safe place.
It is strongly recommended to have a known-good standalone SPI-flash programmer.

TOOLS
=====

# Hardware

1. Screwdriver.
2. A [test-clip].
3. Standalone SPI-flash programmer capable to interface with 1.8V chips
   like e.g. the [Bus_Blaster] with a 1.8V power supply like the [LDL1117S18R].

[test-clip]: https://www.pomonaelectronics.com/products/test-clips/soic-clip-8-pin
[Bus_Blaster]: http://dangerousprototypes.com/docs/Bus_Blaster
[LDL1117S18R]: https://www.st.com/en/power-management/ldl1117.html

# Software

1. arm-none-eabi-gcc
2. mkimage from uboot-tools
3. standard system and developer utilities

PROCEDURE
=========

First, if your device has 2GB of memory, you need to apply `cb/2g.patch` and
fix the `/memory` node in `rk3399-gru-kevin.patch`. *This has not been tested!*

Edit the TOOLCHAIN_DIR variable in the makefile to adjust for your system.
For example if the native cc is for aarch64, you can leave it commented out.

Then, with all prerequisites in place, do:

    make -j$(nproc)

This will generate `release/flash.img`, the image to write to the SPI flash memory, and
`release/spiflash`, the program to write it from within the device itself.

Disable WP and First time Flashing
----------------------------------

To disable the Write Protect (WP) bits, a version of flashrom with support for the `--wp-disable` option is needed.
The upstream version is NOT one of them, but the one preinstalled in ChromeOS is.

The WP bits are protected with the WP pin. To access the WP pin remove the backcover of the device.
It has been said that disconnecting the battery is enough to disable the WP pin, but it did not work for me.
Another option is to shortcircuit it to VCC with a wire from pin-3 to pin-8 with the [test-clip].

So, with either option to disable the WP pin, execute `flashrom --wp-disable` and check with `flashrom --wp-status`.

Then, with the chromebook powered off, use your external programmer
of choice connected to the [test-clip] to write `release/flash.img`.

Flashing
--------

From Linux on the chromebook, just do:

    make flash

KEYBOARD LAYOUT
===============

The keyboard lacks keys like DEL, HOME, PGUP, ...
To fix this, there is a patch for the kernel that modifies the keyboard layout this way:
1. The 'search' button becomes 'Caps Lock'
2. The right 'alt' button becomes 'Meta'
3. The upper row layout is in the keys.svg image

LICENSE
=======

There is readme file specifying the applicable license for each subdirectory.

All files in this directory are licensed under the 0BSD license (BSD Zero Clause License).

https://spdx.org/licenses/0BSD.html
