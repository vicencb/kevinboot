#include "gpio.h"

static inline uint32_t rk_clrsetreg(uint32_t clr, uint32_t set) {
  return ((clr | set) << 16) | set;
}

static uint32_t volatile * const pmugrf_gpio0a_p = (void *)0xFF320040;
static uint32_t volatile * const grf_gpio2a_p    = (void *)0xFF77E040;
static struct {
  uint32_t swporta_dr;
  uint32_t swporta_ddr;
} volatile * const gpio_port[] = {
  (void *)0xFF720000,
  (void *)0xFF730000,
  (void *)0xFF780000,
  (void *)0xFF788000,
  (void *)0xFF790000,
};

static void gpio_dis_pull(union gpio gpio) {
  uint32_t dis_pull = rk_clrsetreg(3 << (gpio.idx * 2), 0);
  if (gpio.blk < 8) { pmugrf_gpio0a_p[gpio.blk - 0] = dis_pull; }
  else              { grf_gpio2a_p   [gpio.blk - 8] = dis_pull; }
}

void gpio_set(union gpio gpio, int value) {
  if (value) { gpio_port[gpio.port]->swporta_dr |=  (1 << gpio.num); }
  else       { gpio_port[gpio.port]->swporta_dr &= ~(1 << gpio.num); }
}

int gpio_get(union gpio gpio) {
  return 1 & (gpio_port[gpio.port]->swporta_dr >> gpio.num);
}

void gpio_output(union gpio gpio, int value) {
  gpio_set(gpio, value);
  gpio_port[gpio.port]->swporta_ddr |= 1 << gpio.num;
  gpio_dis_pull(gpio);
}

void gpio_input(union gpio gpio) {
  gpio_dis_pull(gpio);
  gpio_port[gpio.port]->swporta_ddr &= ~(1 << gpio.num);
}
