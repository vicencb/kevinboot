#include <stdint.h>
#include "gpio.h"
#include "pwm.h"

static inline uint32_t rk_clrsetreg(uint32_t clr, uint32_t set) {
  return ((clr | set) << 16) | set;
}

static inline void set_pwm(unsigned num, unsigned min, unsigned now, unsigned max) {
  pwm(num, 0x10000ul * (max - now) / (max - min));
}

static uint32_t volatile * const grf_io_vsel         = (void *)0xFF77E640;
static uint32_t volatile * const pmugrf_soc_con0     = (void *)0xFF320180;
static uint32_t volatile * const pmugrf_gpio0b_iomux = (void *)0xFF320004;

void power_init(void) {
  *grf_io_vsel         = rk_clrsetreg(3 << 0, 0     );
  *pmugrf_soc_con0     = rk_clrsetreg(3 << 8, 0     );
  *pmugrf_gpio0b_iomux = rk_clrsetreg(0     , 3 << 2);

  gpio_output(GPIO(0, B, 4), 1); // P30V_EN
  gpio_output(GPIO(0, B, 2), 1); // P15V_EN
  gpio_output(GPIO(1, B, 3), 0); // wlan reset

  set_pwm(2, 799065, 1150000, 1303738); // LIT
  set_pwm(1, 798674,  800000, 1302172); // BIG
  set_pwm(0, 785782,  800000, 1217729); // GPU
  set_pwm(3, 800069,  925000, 1049692); // LOG
}
