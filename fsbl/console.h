#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdint.h>

void console_init(void);
void print_chr(int);
void print_str(const char *);
void print_x32(uint32_t);

#endif
