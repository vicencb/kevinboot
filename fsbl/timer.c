#include <stdint.h>
#include "timer.h"

static struct {
  uint32_t load_count0;
  uint32_t load_count1;
  uint32_t cur_value0;
  uint32_t cur_value1;
  uint32_t load_count2;
  uint32_t load_count3;
  uint32_t int_status;
  uint32_t ctrl_reg;
} volatile * const timer0_ptr = (void *)0xFF850000;

int timer_get() {
  return timer0_ptr->cur_value0;
}

void timer_init() {
  timer0_ptr->load_count0 = 0xFFFFFFFF;
  timer0_ptr->load_count1 = 0xFFFFFFFF;
  timer0_ptr->load_count2 = 0;
  timer0_ptr->load_count3 = 0;
  timer0_ptr->ctrl_reg    = 1;
}

void delay(int t) {
  int x = timer_get() + t;
  while (!late(x));
}
