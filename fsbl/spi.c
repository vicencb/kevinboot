#include <stdint.h>
#include "spi.h"
#include "console.h"

static inline uint32_t rk_clrsetreg(uint32_t clr, uint32_t set) {
  return ((clr | set) << 16) | set;
}

union ctrlr0 {
  uint32_t all;
  struct {
    uint32_t dfs  :2; // Data Frame Size
    uint32_t cfs  :4;
    uint32_t scph :1;
    uint32_t scpol:1;
    uint32_t csm  :2;
    uint32_t ssd  :1; // SSN to Sclk_out delay
    uint32_t em   :1;
    uint32_t fbm  :1;
    uint32_t bht  :1; // Byte and Halfword Transform
    uint32_t rsd  :2; // Rxd Sample Delay
    uint32_t frf  :2;
    uint32_t drx  :1; // xfm (transfer mode): disable RX
    uint32_t dtx  :1; // xfm (transfer mode): disable TX
  };
};
struct sr {
  uint32_t bsf :1;
};
static struct {
  union ctrlr0 ctrlr0         ;
  uint32_t     ctrlr1         ;
  uint32_t     spienr         ;
  uint32_t     ser            ;
  uint32_t     baudr          ;
  uint32_t     txftlr         ;
  uint32_t     rxftlr         ;
  uint32_t     txflr          ;
  uint32_t     rxflr       : 6;
  struct sr    sr             ;
  uint32_t     ipr            ;
  uint32_t     imr            ;
  uint32_t     isr            ;
  uint32_t     risr           ;
  uint32_t     icr            ;
  uint32_t     dmacr          ;
  uint32_t     damtdlr        ;
  uint32_t     damrdlr        ;
  uint32_t     reserved0[0xEE];
  uint32_t     txdr           ;
  uint32_t     reserved1[0xFF];
  uint32_t     rxdr           ;
} volatile *const spi = (void*)0xFF1D0000;

#define SPI_SRCCLK_HZ 99000000
#define SCLK_FREQ     49500000
void spi_init() {
  spi->baudr = SPI_SRCCLK_HZ / SCLK_FREQ;
}

static inline void spi_put(const uint8_t *buf, unsigned cnt) {
  union ctrlr0 ctrlr0 = { .all = spi->ctrlr0.all };
  ctrlr0.drx = 1;
  ctrlr0.dtx = 0;
  spi->ctrlr0.all = ctrlr0.all;
  spi->ctrlr1 = cnt - 1;

  spi->spienr = 1;
  while (cnt--) {
    spi->txdr = *buf++;
  }
  while (spi->sr.bsf);
  spi->spienr = 0;
}

static inline void spi_get(uint8_t *buf, unsigned cnt) {
  union ctrlr0 ctrlr0 = { .all = spi->ctrlr0.all };
  ctrlr0.drx = 0;
  ctrlr0.dtx = 1;
  spi->ctrlr0.all = ctrlr0.all;
  spi->ctrlr1 = cnt - 1;

  spi->spienr = 1;
  while (cnt) {
    unsigned rxflr = spi->rxflr;
    cnt -= rxflr;
    while (rxflr--) { *buf++ = spi->rxdr; }
  }
  while (spi->sr.bsf);
  spi->spienr = 0;
}

static inline void spi_cmd(uint32_t addr, uint8_t *dato, unsigned leno, void *dati, unsigned leni) {
  dato[1] = addr >> 16;
  dato[2] = addr >> 8;
  dato[3] = addr >> 0;
  spi->ser = 1;
  spi_put(dato, leno);
  spi_get(dati, leni);
  spi->ser = 0;
}

void spi_read(unsigned off, unsigned len, void *dat) {
  uint8_t cmd[5] = {0x0B};
  while (len) {
    unsigned xfer_len = len >= 0x10000 ? 0x10000 : len;
    spi_cmd(off, cmd, sizeof(cmd), dat, xfer_len);
    off += xfer_len;
    dat += xfer_len;
    len -= xfer_len;
  }
}
