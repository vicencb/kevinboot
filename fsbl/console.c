#include "console.h"
#include "uart.h"
#include "memconsole.h"

enum {
  CONSOLE_NONE = 0 << 0, // Be silent
  CONSOLE_UART = 1 << 0, // Print messages to UART
  CONSOLE_MEMC = 1 << 1, // Save messages in memory

  CONSOLE = CONSOLE_UART
//CONSOLE = CONSOLE_NONE
};

void console_init(void) {
  if (CONSOLE & CONSOLE_UART) { uart_init(); }
  if (CONSOLE & CONSOLE_MEMC) { memc_init(); }
}

void print_chr(int chr) {
  if (CONSOLE & CONSOLE_UART) { uart_print(chr); }
  if (CONSOLE & CONSOLE_MEMC) { memc_print(chr); }
}

void print_str(const char *str) {
  while (*str) { print_chr(*str++); }
}

void print_x32(uint32_t hex) {
  for (int i = sizeof(hex) * 8 - 4; i >= 0; i -= 4) {
    int c = (hex >> i) & 0xF;
    c += c >= 0xA ? 'A' - 0xA : '0';
    print_chr(c);
  }
}
