#ifndef RESET_H
#define RESET_H

__attribute__((noreturn)) void hard_reset();
__attribute__((noreturn)) void hard_halt();

#endif
