#include <stdint.h>
#include "clock.h"
#include "timer.h"

static inline uint32_t rk_clrsetreg(uint32_t clr, uint32_t set) {
  return ((clr | set) << 16) | set;
}

static struct {
  uint32_t apll_l_con[6];
  uint32_t reserved0 [2];
  uint32_t apll_b_con[6];
  uint32_t reserved1 [2];
  uint32_t dpll_con  [6];
  uint32_t reserved2 [2];
  uint32_t cpll_con  [6];
  uint32_t reserved3 [2];
  uint32_t gpll_con  [6];
  uint32_t reserved4 [2];
  uint32_t npll_con  [6];
  uint32_t reserved5 [2];
  uint32_t vpll_con  [6];
  uint32_t reserved6 [10];
  uint32_t clksel_con[4];
} volatile * const cru_ptr = (void *)0xFF760000;

enum {
  OSC_HZ          =  24000000,
  GPLL_HZ         = 594000000,
  CPLL_HZ         = 800000000,
  PPLL_HZ         = 676000000,

  PMU_PCLK_HZ     =  96571428,

  ACLKM_CORE_HZ   = 300000000,
  ATCLK_CORE_HZ   = 300000000,
  PCLK_DBG_HZ     = 100000000,

  PERIHP_ACLK_HZ  = 148500000,
  PERIHP_HCLK_HZ  = 148500000,
  PERIHP_PCLK_HZ  =  37125000,

  PERILP0_ACLK_HZ =  99000000,
  PERILP0_HCLK_HZ =  99000000,
  PERILP0_PCLK_HZ =  49500000,

  PERILP1_HCLK_HZ =  99000000,
  PERILP1_PCLK_HZ =  99000000,

  // PLL_CON0
  PLL_FBDIV_SHIFT       = 0,
  PLL_FBDIV_MASK        = 0xFFF << PLL_FBDIV_SHIFT,

  // PLL_CON1
  PLL_POSTDIV2_SHIFT    = 12,
  PLL_POSTDIV2_MASK     = 0x7 << PLL_POSTDIV2_SHIFT,
  PLL_POSTDIV1_SHIFT    = 8,
  PLL_POSTDIV1_MASK     = 0x7 << PLL_POSTDIV1_SHIFT,
  PLL_REFDIV_SHIFT      = 0,
  PLL_REFDIV_MASK       = 0x3F << PLL_REFDIV_SHIFT,

  PLL_LOCK_STATUS_SHIFT = 31,
  PLL_LOCK_STATUS_MASK  = 1 << PLL_LOCK_STATUS_SHIFT,

  // PLL_CON3
  PLL_MODE_SHIFT        = 8,
  PLL_MODE_MASK         = 3 << PLL_MODE_SHIFT,
  PLL_MODE_SLOW         = 0 << PLL_MODE_SHIFT,
  PLL_MODE_NORM         = 1 << PLL_MODE_SHIFT,
  PLL_DSMPD_SHIFT       = 3,
  PLL_DSMPD_MASK        = 1 << PLL_DSMPD_SHIFT,
  PLL_INTEGER_MODE      = 1 << PLL_DSMPD_SHIFT,

  // CLKSEL_CON
  CLK_DIV_SHIFT         = 8,
  CLK_DIV_MASK          = 0x1F << CLK_DIV_SHIFT,
  CLK_CORE_DIV_SHIFT    = 0,
  CLK_CORE_DIV_MASK     = 0x1F << CLK_CORE_DIV_SHIFT,
  CLK_CORE_SEL_SHIFT    = 6,
  CLK_CORE_SEL_MASK     = 3 << CLK_CORE_SEL_SHIFT,
  CLK_CORE_SEL_ALPLL    = 0 << CLK_CORE_SEL_SHIFT,
  CLK_CORE_SEL_ABPLL    = 1 << CLK_CORE_SEL_SHIFT,
};

static void rkclk_set_pll(uint32_t volatile *pll_con, uint32_t fbdiv, uint32_t postdiv1) {
  // stable clock
  pll_con[3] = rk_clrsetreg(PLL_MODE_MASK, PLL_MODE_SLOW);

  // integer mode
  pll_con[3] = rk_clrsetreg(PLL_DSMPD_MASK, PLL_INTEGER_MODE);

  pll_con[0] = rk_clrsetreg(PLL_FBDIV_MASK, fbdiv << PLL_FBDIV_SHIFT);
  pll_con[1] = rk_clrsetreg(
         PLL_POSTDIV2_MASK  |             PLL_POSTDIV1_MASK  |      PLL_REFDIV_MASK,
    1 << PLL_POSTDIV2_SHIFT | postdiv1 << PLL_POSTDIV1_SHIFT | 1 << PLL_REFDIV_SHIFT
  );

  // wait lock
  while (!(pll_con[2] & PLL_LOCK_STATUS_MASK)) {
    udelay(1);
  }

  // normal mode
  pll_con[3] = rk_clrsetreg(PLL_MODE_MASK, PLL_MODE_NORM);
}

static void configure_cpu(int big) {
  uint32_t aclkm_div, atclk_div, pclk_dbg_div;
  uint32_t apll_hz  = 1512000000;
  uint32_t postdiv1 = 1;
  uint32_t fbdiv    = apll_hz * postdiv1 / OSC_HZ;
  uint32_t con_base = 0;
  uint32_t parent   = CLK_CORE_SEL_ALPLL;
  uint32_t volatile *pll_con = cru_ptr->apll_l_con;

  if (big) {
    apll_hz  = 600000000;
    postdiv1 = 3;
    fbdiv    = apll_hz * postdiv1 / OSC_HZ;
    con_base = 2;
    parent   = CLK_CORE_SEL_ABPLL;
    pll_con  = cru_ptr->apll_b_con;
  }

  rkclk_set_pll(pll_con, fbdiv, postdiv1);
  aclkm_div    = (apll_hz + ACLKM_CORE_HZ - 1) / ACLKM_CORE_HZ - 1;
  atclk_div    = (apll_hz + ATCLK_CORE_HZ - 1) / ATCLK_CORE_HZ - 1;
  pclk_dbg_div = (apll_hz + PCLK_DBG_HZ   - 1) / PCLK_DBG_HZ   - 1;

  cru_ptr->clksel_con[con_base] = rk_clrsetreg(
                 CLK_DIV_MASK  | CLK_CORE_SEL_MASK |      CLK_CORE_DIV_MASK,
    aclkm_div << CLK_DIV_SHIFT | parent            | 0 << CLK_CORE_DIV_SHIFT
  );

  cru_ptr->clksel_con[con_base + 1] = rk_clrsetreg(
                    CLK_DIV_MASK  |              CLK_CORE_DIV_MASK,
    pclk_dbg_div << CLK_DIV_SHIFT | atclk_div << CLK_CORE_DIV_SHIFT
  );
}

void rkclk_configure_cpu() {
  configure_cpu(0);
  configure_cpu(1);
}

void rkclk_configure_ddr() {
  static uint32_t volatile *const rk3399_pmusgrf_ddr_rgn_con = (void *)0xFF330000;
  rk3399_pmusgrf_ddr_rgn_con[16] = 0xC000C000;
  rkclk_set_pll(&cru_ptr->dpll_con[0], 116, 3); // 933MHz
}
