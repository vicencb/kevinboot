#include "gpio.h"
#include "reset.h"

__attribute__((noreturn)) void hard_reset() {
  gpio_output(GPIO(0, B, 3), 1);
  while (1);
}

__attribute__((noreturn)) void hard_halt() {
  gpio_output(GPIO(1, A, 6), 1);
  while (1);
}
