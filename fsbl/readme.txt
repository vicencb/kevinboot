All files in this directory are licensed under the 0BSD license (BSD Zero Clause License).
https://spdx.org/licenses/0BSD.html

Some of the aforementioned files are inspired by code from u-boot, coreboot and linux.
