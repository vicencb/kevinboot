#include <stdint.h>
#include "gpio.h"
#include "pwm.h"

// static inline uint32_t rk_clrsetreg(uint32_t clr, uint32_t set) {
//   return ((clr | set) << 16) | set;
// }

static struct {
  uint32_t  cnt;
  uint32_t  period_hpr;
  uint32_t  duty_lpr;
  uint32_t  ctrl;
} volatile *const regs = (void *)0xFF420000;

static uint32_t volatile *const pmugrf_gpio0a_iomux = (void *)0xFF320000;
static uint32_t volatile *const pmugrf_gpio1c_iomux = (void *)0xFF320018;
static uint32_t volatile *const    grf_gpio4c_iomux = (void *)0xFF77E028;

void pwm(unsigned num, unsigned duty0x10000) {
  enum {
    PWM_ENABLE           = 1 << 0,
    PWM_CONTINUOUS       = 1 << 1,
    PWM_DUTY_POSTIVE     = 1 << 3,
    PWM_INACTIVE_POSTIVE = 1 << 4,
  };
  static const unsigned clock_hz  = 96571428;
  static const unsigned period_ns = 3337;
  static const unsigned period    = (clock_hz / 1000) * period_ns / 1000000;
  unsigned duty = (period * duty0x10000 + 0x8000) / 0x10000;

  regs[num].ctrl       = PWM_CONTINUOUS | PWM_DUTY_POSTIVE | PWM_INACTIVE_POSTIVE;
  regs[num].period_hpr = period;
  regs[num].duty_lpr   = duty;
  regs[num].ctrl      |= PWM_ENABLE;
  
  static const union gpio gpio[] = {GPIO(4, C, 2), GPIO(4, C, 6), GPIO(1, C, 3), GPIO(0, A, 6)};
  gpio_input(gpio[num]);  /* remove pull */

  static uint32_t volatile *const iomux_reg[] = {
       grf_gpio4c_iomux,
       grf_gpio4c_iomux,
    pmugrf_gpio1c_iomux,
    pmugrf_gpio0a_iomux,
  };
  static const uint32_t iomux_val[] = {
    ((1 <<  4) << 16) | (1 <<  4),  // rk_clrsetreg(0, 1 <<  4),
    ((1 << 12) << 16) | (1 << 12),  // rk_clrsetreg(0, 1 << 12),
    ((1 <<  6) << 16) | (1 <<  6),  // rk_clrsetreg(0, 1 <<  6),
    ((1 << 12) << 16) | (1 << 12),  // rk_clrsetreg(0, 1 << 12),
  };
  *iomux_reg[num] = iomux_val[num];
}
