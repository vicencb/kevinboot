#include <stddef.h>
#include <stdint.h>
#include "memconsole.h"

struct coreboot_table_header {
  char     signature[4]   ;
  uint32_t header_bytes   ;
  uint32_t header_checksum;
  uint32_t table_bytes    ;
  uint32_t table_checksum ;
  uint32_t table_entries  ;
};

struct lb_cbmem_ref {
  uint32_t tag       ;
  uint32_t size      ;
  uint64_t cbmem_addr;
};

enum {
  CB_TAG_CBMEM_CONSOLE = 0x17,
  CB_SIZ_CBMEM_CONSOLE = 0x1000 - sizeof(struct coreboot_table_header) - sizeof(struct lb_cbmem_ref) - 8,
};

struct cbmem_cons {
  uint32_t size     ;
  uint32_t cursor   :28;
  uint32_t          : 3;
  uint32_t overflow : 1;
  uint8_t  body[CB_SIZ_CBMEM_CONSOLE];
};

struct coreboot_table {
  struct coreboot_table_header coreboot_table_header;
  struct lb_cbmem_ref          lb_cbmem_ref         ;
  struct cbmem_cons            cbmem_cons           ;
};

static struct coreboot_table *coreboot_table = (void*)0x1FF000;

void memc_init(void) {
  *coreboot_table = (struct coreboot_table){
    .coreboot_table_header = {
      .signature     = "LBIO",
      .header_bytes  = sizeof(struct coreboot_table_header),
      .table_bytes   = sizeof(struct lb_cbmem_ref),
      .table_entries = 1,
    },
    .lb_cbmem_ref = {
      .tag        = CB_TAG_CBMEM_CONSOLE,
      .size       = sizeof(struct lb_cbmem_ref),
      .cbmem_addr = (uint64_t)&coreboot_table->cbmem_cons,
    },
    .cbmem_cons = {
      .size = sizeof(coreboot_table->cbmem_cons.body),
    },
  };
}

void memc_print(int chr) {
  uint32_t cursor = coreboot_table->cbmem_cons.cursor;
  coreboot_table->cbmem_cons.body[cursor++] = chr;
  if (cursor == sizeof(coreboot_table->cbmem_cons.body)) {
    coreboot_table->cbmem_cons.overflow = 1;
    coreboot_table->cbmem_cons.cursor   = 0;
  } else {
    coreboot_table->cbmem_cons.cursor   = cursor;
  }
}
