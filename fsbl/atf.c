#include <stdint.h>
#include "atf.h"
#include "gpio.h"
#include "plat_params_exp.h"

#define ATF_PARAM_EP            1
#define ATF_PARAM_BL31          3
#define ATF_VERSION_1           1
#define ATF_EP_NON_SECURE       1
#define MODE_RW_SHIFT           4
#define MODE_RW_64              0
#define MODE_EL_SHIFT           2
#define MODE_EL_MASK            3
#define MODE_EL2                2
#define MODE_SP_SHIFT           0
#define MODE_SP_MASK            1
#define MODE_SP_ELX             1
#define SPSR_DAIF_SHIFT         6
#define SPSR_DAIF_MASK          15
#define DAIF_FIQ_BIT            (1<<0)
#define DAIF_IRQ_BIT            (1<<1)
#define DAIF_ABT_BIT            (1<<2)
#define DAIF_DBG_BIT            (1<<3)
#define DISABLE_ALL_EXECPTIONS	(DAIF_FIQ_BIT | DAIF_IRQ_BIT | DAIF_ABT_BIT | DAIF_DBG_BIT)
#define SPSR_64(el, sp, daif)   ( \
  MODE_RW_64                << MODE_RW_SHIFT  | \
  ((el  ) & MODE_EL_MASK  ) << MODE_EL_SHIFT  | \
  ((sp  ) & MODE_SP_MASK  ) << MODE_SP_SHIFT  | \
  ((daif) & SPSR_DAIF_MASK) << SPSR_DAIF_SHIFT  \
)
struct param_header {
  uint8_t  type;    /* type of the structure */
  uint8_t  version; /* version of this structure */
  uint16_t size;    /* size of this structure in bytes */
  uint32_t attr;    /* attributes: unused bits SBZ */
};
struct entry_point_info {
  struct param_header h;
  uintptr_t           pc;
  uint32_t            spsr;
  unsigned long       arg[8];
};
struct image_info {
  struct param_header h;
  uintptr_t           image_base; /* physical address of base of image */
  uint32_t            image_size; /* bytes read from image file */
};
struct bl31_params {
  struct param_header      h;
  struct image_info       *bl31_image_info;
  struct entry_point_info *bl32_ep_info;
  struct image_info       *bl32_image_info;
  struct entry_point_info *bl33_ep_info;
  struct image_info       *bl33_image_info;
};
struct bl31_params_mem {
  struct bl31_params      bl31_params;
  struct entry_point_info bl33_ep_info;
};

static struct bl31_params_mem bl31_params_mem = {
  .bl31_params     = {
    .h               = {
      .type            = ATF_PARAM_BL31,
      .version         = ATF_VERSION_1,
      .size            = sizeof(struct bl31_params),
    },
    .bl33_ep_info    = &bl31_params_mem.bl33_ep_info,
  },
  .bl33_ep_info    = {
    .h               = {
      .type            = ATF_PARAM_EP,
      .version         = ATF_VERSION_1,
      .size            = sizeof(struct entry_point_info),
      .attr            = ATF_EP_NON_SECURE,
    },
    .pc              = 0x200000,
    .spsr            = SPSR_64(MODE_EL2, MODE_SP_ELX, DISABLE_ALL_EXECPTIONS),
    .arg             = { 0x100000 },
  },
};

static struct bl_aux_param_gpio param_p15_en = {
  .h = { .type = BL_AUX_PARAM_RK_SUSPEND_GPIO, .next = 0 },
  .gpio = { .index = GPIO_RAW(0, B, 2) },
};
static struct bl_aux_param_gpio param_p18_audio_en = {
  .h = { .type = BL_AUX_PARAM_RK_SUSPEND_GPIO, .next = (uint64_t)&param_p15_en.h },
  .gpio = { .index = GPIO_RAW(0, A, 2) },
};
static struct bl_aux_param_gpio param_p30_en = {
  .h = { .type = BL_AUX_PARAM_RK_SUSPEND_GPIO, .next = (uint64_t)&param_p18_audio_en.h },
  .gpio = { .index = GPIO_RAW(0, B, 4) },
};
static struct bl_aux_param_gpio param_reset = {
  .h = { .type = BL_AUX_PARAM_RK_RESET_GPIO, .next = (uint64_t)&param_p30_en.h },
  .gpio = { .index = GPIO_RAW(0, B, 3), .polarity = 1 },
};
static struct bl_aux_param_gpio param_poweroff = {
  .h = { .type = BL_AUX_PARAM_RK_POWEROFF_GPIO, .next = (uint64_t)&param_reset.h },
  .gpio = { .index = GPIO_RAW(1, A, 6), .polarity = 1 },
};
static struct bl_aux_param_rk_apio param_apio = {
  .h = { .type = BL_AUX_PARAM_RK_SUSPEND_APIO, .next = (uint64_t)&param_poweroff.h },
  .apio = { .apio1 = 1, .apio2 = 1, .apio3 = 1, .apio4 = 1, .apio5 = 1 },
};

void next_stage(void) {
  void (*bl31_entry)(struct bl31_params *params, void *plat_params) = (void*)0x40000;
  bl31_entry(&bl31_params_mem.bl31_params, &param_apio.h);
}
