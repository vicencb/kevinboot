#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>

union gpio {
  uint32_t raw;
  struct {
    uint32_t idx  :3;
    uint32_t bank :2;
    uint32_t port :4;
  };
  struct {
    uint32_t num  :5;
  };
  struct {
    uint32_t      :3;
    uint32_t blk  :6;
  };
};

enum { GPIO_A, GPIO_B, GPIO_C, GPIO_D };

#define GPIO(p, b, i) ((union gpio){.port = p, .bank = GPIO_##b, .idx = i})
#define GPIO_RAW(p, b, i) (((p) << 5) | ((GPIO_##b) << 3) | (i))

void gpio_output(union gpio gpio, int value);
void gpio_input (union gpio gpio);
void gpio_set   (union gpio gpio, int value);
int  gpio_get   (union gpio gpio);

#endif
