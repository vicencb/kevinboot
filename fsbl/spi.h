#ifndef SPI_H
#define SPI_H

void spi_init();
void spi_read(unsigned off, unsigned len, void *);

#endif
