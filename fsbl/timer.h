#ifndef TIMER_H
#define TIMER_H

#define TIMER_SCALE 24

void timer_init();
int  timer_get();
void delay(int);

static inline void udelay (int x) { delay(x * TIMER_SCALE); }
static inline int  ufutur (int x) { return timer_get() + x * TIMER_SCALE; }
static inline int  late   (int x) { return x - timer_get() <= 0; }
static inline int  time_ms(int x) { return x / (TIMER_SCALE * 1000); }

#endif
