#include <stdint.h>
#include <string.h>
#include "atf.h"
#include "clock.h"
#include "console.h"
#include "crypto_hash.h"
#include "gpio.h"
#include "power.h"
#include "reset.h"
#include "sdram.h"
#include "spi.h"
#include "timer.h"

enum {
  TIMED_BENCHMARK = 0,  // Debug option.
  DISABLE_CHECK   = 1,  // Be Faster.
  BLK_ALIGN = 4096,
  PAYLOAD = 0x00020000, // SPI payload starts after the reserved space for us.
  Z_SIZE  = 0x01000000, // SPI read command uses a 3-byte address, so, nothing will be bigger than 16MB.
  Z_LOAD  = 0x04000000, // Temporary buffer for compressed data.
};

struct segment {
  int32_t  x_size; // Extracted size.
  int32_t  z_size; // Compressed size.
  uint64_t addr  ; // Load address.
  uint32_t flags ;
  uint32_t reserved;
  uint8_t  hash[crypto_hash_BYTES]; // Integrity check.
};

static int decodeLz4Block(const char *src, char *dst, int compressedSize, int dstCapacity) {
  uint8_t const *inp = (const void *)src;
  int j = 0;
  for (int i = 0; i < compressedSize;) {
    int token = inp[i++];

    // Literals
    int literals_length = token >> 4;
    if (literals_length) {
      int l = literals_length + 240;
      while (l == 255) {
        l = inp[i++];
        literals_length += l;
      }

      // Copy the literals
      if (j + literals_length > dstCapacity) { return -1; }
      memcpy(dst + j, inp + i, literals_length);
      j += literals_length;
      i += literals_length;

      // End of buffer?
      if (i == compressedSize) { return j; }
    }

    // Match copy
    // 2 bytes offset (little endian)
    int offset = inp[i++];
    offset += inp[i++] << 8;

    if (0 && !offset) { continue; } // sequence of just literals is end of block
    if (!(offset && offset <= j)) { return -1; } // 0 is an invalid offset value

    // length of match copy
    int match_length = token & 15;
    int l = match_length + 240;
    while (l == 255) {
      l = inp[i++];
      match_length += l;
    }

    // Copy the match
    int pos = j - offset; // position of the match copy in the current output
    int end = j + match_length + 4; // minmatch = 4
    if (end >= dstCapacity) { return -1; }
    while (j < end) { dst[j++] = dst[pos++]; }
  }
  return j;
}

int main() {
  uint32_t volatile *const cru_glb_rst_st = (void *)0xFF760514;
  if (*cru_glb_rst_st) { hard_reset(); }
  timer_init();
  power_init();
  udelay(200);
  rkclk_configure_cpu();
  sdram_init(&sdram_params);
  console_init();
  spi_init();

  struct segment seg;
  int off = PAYLOAD;
  int a, b;
  if (TIMED_BENCHMARK) { b = timer_get(); }
  while (1) {
    spi_read(off, sizeof(seg), &seg);
    off += sizeof(seg);
    if (!~seg.x_size) { break; }
    print_str("Load: 0x"); print_x32(seg.addr); print_str(":0x"); print_x32(seg.x_size); print_str(":0x"); print_x32(seg.z_size);
    int next_off = (off + seg.z_size + (BLK_ALIGN - 1)) & ~(BLK_ALIGN - 1);
    if (seg.z_size >= Z_SIZE || next_off >= Z_SIZE) { goto err; }
    unsigned compress = seg.x_size != seg.z_size;
    spi_read(off, seg.z_size, compress ? (void*)Z_LOAD : (void*)seg.addr);
    off = next_off;
    if (TIMED_BENCHMARK) {
      a = b;
      b = timer_get();
      print_str(" 0x"); print_x32(time_ms(b - a)); print_str("ms");
    }

    if (compress && decodeLz4Block((void*)Z_LOAD, (void*)seg.addr, seg.z_size, seg.x_size) < 0) { goto err; }
    if (TIMED_BENCHMARK) {
      a = b;
      b = timer_get();
      print_str(" 0x"); print_x32(time_ms(b - a)); print_str("ms");
    }

    if (!DISABLE_CHECK) {
      uint64_t hash[crypto_hash_BYTES / 8];
      crypto_hash((void*)hash, (void*)seg.addr, seg.x_size);
      if (memcmp(hash, seg.hash, sizeof(hash))) { goto err; }
      if (TIMED_BENCHMARK) {
        a = b;
        b = timer_get();
        print_str(" 0x"); print_x32(time_ms(b - a)); print_str("ms");
      }
    }

    print_chr('\n');
  }

  next_stage();

err:
  print_str("\nError!\n");
  udelay(0x100000);
  hard_halt();
  return 0;
}
