#include <stdint.h>
#include "uart.h"

#define BAUD  1500000
#define CLOCK 24000000
#define BASE  0xFF1A0000

static inline uint32_t rk_clrsetreg(uint32_t clr, uint32_t set) {
  return ((clr | set) << 16) | set;
}

static inline void soc_uart_init(void) {
  enum {
    GRF_GPIO4C3_SEL_MASK = 3 << 6,
    GRF_UART2DGBC_SIN    = 1 << 6,
    GRF_GPIO4C4_SEL_MASK = 3 << 8,
    GRF_UART2DBGC_SOUT   = 1 << 8,
  };
  uint32_t volatile *const grf_gpio4c_iomux = (void *)0xFF77E028;
  *grf_gpio4c_iomux = rk_clrsetreg(
    GRF_GPIO4C3_SEL_MASK | GRF_GPIO4C4_SEL_MASK,
    GRF_UART2DGBC_SIN    | GRF_UART2DBGC_SOUT
  );
}

static struct {
  union {
    uint32_t thr; /* lcr.BKSE == 0 */
    uint32_t dll; /* lcr.BKSE == 1 */
  };
  uint32_t dlm;
  uint32_t fcr;
  uint32_t lcr;
  uint32_t mcr;
  uint32_t lsr;
} volatile *const uart = (void *)BASE;

enum {
  FCR_FIFO_EN = 0x01, /* FIFO enable */
  FCR_RXSR    = 0x02, /* Receiver soft reset */
  FCR_TXSR    = 0x04, /* Transmitter soft reset */
  LCR_8N1     = 0x03, /* 8 data, 1 stop, no parity */
  LCR_BKSE    = 0x80, /* Bank select enable */
  LSR_THRE    = 0x20, /* transmit holding register empty */
};

void uart_init(void) {
  soc_uart_init();
  unsigned baud_divisor = ( CLOCK + 8 * BAUD ) / ( 16 * BAUD );
  uart->fcr = FCR_FIFO_EN | FCR_RXSR | FCR_TXSR;
  uart->lcr = LCR_8N1 | LCR_BKSE;
  uart->dll = baud_divisor & 0xFF;
  uart->dlm = (baud_divisor >> 8) & 0xFF;
  uart->lcr = LCR_8N1;
}

void uart_print(int chr) {
  while (!(uart->lsr & LSR_THRE));
  uart->thr = chr;
}
